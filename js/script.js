var balls = document.getElementsByClassName("ball");
document.onmousemove = function(){
  var x = event.clientX * 100 / window.innerWidth + "%";
  var y = event.clientY * 100 / window.innerHeight + "%";

  for(var i=0;i<2;i++){
    balls[i].style.left = x;
    balls[i].style.top = y;
    balls[i].style.transform = "translate(-"+x+",-"+y+")";
  }
}

var colors = ["#23CE6B", "#5386E4", "#ED315D", "#CBFF47", "#7E3F8F", "#027C8D", "#BCAF9C", "#FF8811"];
var selectedColor = 0;

$(document).ready(function() {
	document.documentElement.style.setProperty("--color", colors[getRandomInt(colors.length)]);
	var location = window.location.href;

	$("#low-header a").each(function () {
		var linkPage = this.href;

		if (location == linkPage) {
			$(this).closest("div").addClass("active");
		}
	});	
});

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}